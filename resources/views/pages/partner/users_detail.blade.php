@extends('layouts.app')

@section('title','PPTKIS - Detail CPMI')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('partner')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('partner-user')}}">CPMI</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Lengkap - {{$data->real_name}} </li>
        </ol>
    </nav>

    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}

    <div class="card shadow mb-4">
        <div class="card-header py-3">

        </div>
        <div class="card-body">
        <form method="POST" action="{{ route('daftar') }}">
            @csrf
            <input type="hidden" name="role_id" value="2">
            <div class="form-group">
                <label for="">Email <span style="color:red">*</span> </label>
                <input disabled type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ $data->user->email }}" placeholder="nama@domain.com" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            {{-- biodata --}}
            <hr>
            <p style="text-align:center">Data Lengkap</p>
            <div class="form-group">
                <label for="">Nomer KTP <span style="color:red">*</span> </label>
                <input disabled type="number" min="1" class="form-control form-control-user @error('ktp') is-invalid @enderror" name="ktp" value="{{ $data->card_id }}" placeholder="1122334455" required autocomplete="ktp" autofocus>
                @error('ktp')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Nama Lengkap <span style="color:red">*</span> </label>
                <input disabled type="text" class="form-control form-control-user @error('real_name') is-invalid @enderror" name="real_name" value="{{ $data->real_name }}" placeholder="Hasyim Adnan" required autocomplete="real_name" autofocus>
                @error('real_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Nomer Telepon <span style="color:red">*</span> </label>
                <input disabled type="number" min="1" class="form-control form-control-user @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ $data->phone_number }}" placeholder="082211223344" required autocomplete="phone_number" autofocus>
                @error('phone_number')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Alamat Sesuai KTP <span style="color:red">*</span> </label>
                <textarea disabled name="address" cols="30" rows="5" class="form-control form-control-user @error('address') is-invalid @enderror" placeholder="Jl Permai Raya no. 21" required autocomplete="address" autofocus>{{ $data->address }}</textarea>
                @error('address')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Negara Asal <span style="color:red">*</span> </label>
                <select disabled name="country_employments_id" class="form-control form-control-user @error('country_employments_id') is-invalid @enderror" required autocomplete="country_employments_id" autofocus>
                    <option value="{{$data->country_employments_id}}"> {{$data->country->name}} </option>
                    @foreach ($country as $item)
                        <option value="{{$item->id}}"> {{$item->name}} </option>
                    @endforeach
                </select>
                @error('address')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            {{-- education --}}
            <hr>
            <p style="text-align:center">Pendidikan Terakhir</p>
            <div class="form-group">
                <label for="">Sekolah / Lembaga</label>
                <input disabled type="text" class="form-control form-control-user @error('education_school') is-invalid @enderror" name="education_school" value="{{ $data->education_school_name }}" placeholder="Sekolah KiniKreatif"  autocomplete="education_school" autofocus>
                @error('education_school')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group row">
                <div class="col-lg-6 col-sm-12">
                    <label for="">Tahun Masuk</label>
                    <input disabled type="date" class="form-control form-control-user @error('education_start_year') is-invalid @enderror" name="education_start_year" value="{{ date('Y-m-d', strtotime($data->education_start_year)) }}"  autocomplete="education_start_year" autofocus>
                    @error('education_start_year')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-lg-6 col-sm-12">
                    <label for="">Tahun Kelulusan</label>
                    <input disabled type="date" class="form-control form-control-user @error('education_end_year') is-invalid @enderror" name="education_end_year" value="{{ date('Y-m-d', strtotime($data->education_end_year)) }}"  autocomplete="education_end_year" autofocus>
                    @error('education_end_year')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <hr>
            <p style="text-align:center">Pekerjaan Terakhir</p>
            <div class="form-group">
                <label for="">Nama Perusahaan</label>
                <input disabled type="text" class="form-control form-control-user @error('experiences_company') is-invalid @enderror" name="experiences_company" value="{{ $data->experiences_company }}" placeholder="PT Kini Kreatif"  autocomplete="experiences_company" autofocus>
                @error('experiences_company')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Posisi</label>
                <input disabled type="text" class="form-control form-control-user @error('experiences_position') is-invalid @enderror" name="experiences_position" value="{{ $data->experiences_position }}" placeholder="Juru Mudi / Pengemudi"  autocomplete="experiences_position" autofocus>
                @error('experiences_position')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Bidang Pekerjaan</label>
                <input disabled type="text" class="form-control form-control-user @error('experiences_classification') is-invalid @enderror" name="experiences_classification" value="{{ $data->experiences_classification }}" placeholder="Perusahaan Tambang"  autocomplete="experiences_classification" autofocus>
                @error('experiences_classification')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <a href="{{route('partner-user')}}" class="btn btn-primary btn-user btn-block">Tutup Halaman</a>
        </form>
        </div>
    </div>

@endsection
