@extends('layouts.app')

@section('title','Partner - Lowongan')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Beranda</li>
            <li class="breadcrumb-item active" aria-current="page">Data Lowongan</li>
        </ol>
    </nav>
    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{route('partner-employment-create')}}" style="float:right" class="btn btn-primary">Lowongan Baru</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                    <th style="width:10px">No</th>
                    <th>Judul Lowongan</th>
                    <th>Bidang Pekerjaan</th>
                    <th>Pendapatan</th>
                    <th>Negara Tujuan</th>
                    <th>Pendaftaran Akhir</th>
                    <th>Status</th>
                    <th width="10">Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th style="width:10px">No</th>
                    <th>Judul Lowongan</th>
                    <th>Bidang Pekerjaan</th>
                    <th>Pendapatan</th>
                    <th>Negara Tujuan</th>
                    <th>Pendaftaran Akhir</th>
                    <th>Status</th>
                    <th width="10">Action</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item->title}}</td>
                  <td>{{$item->sector->name}}</td>
                  <td>{{"IDR " . number_format($item->sallery, 0, ",", ".")}}</td>
                  <td>{{$item->country->name}}</td>
                  <td>{{date('d M Y', strtotime($item->end_date))}}</td>
                  <td> <p style="text-transform:capitalize">{{$item->is_publish}}</p></td>
                  <td>
                        <div class="dropdown">
                            <a class="btn btn-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{route('partner-employment-show',$item->id)}}"><i class="fas fa-eye"></i> Detail </a>
                                <a class="dropdown-item" href="{{route('partner-employment-edit',$item->id)}}"><i class="fas fa-pencil-alt"></i> Ubah Lowongan </a>
                            </div>
                        </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection
