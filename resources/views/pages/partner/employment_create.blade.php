@extends('layouts.app')

@section('title','Partner - Tambah Lowongan')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('partner')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('partner-employment')}}">Data Lowongan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Buat Baru</li>
        </ol>
    </nav>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Lowongan Baru</h6>
        </div>
        <div class="card-body">
            <form action="{{route('partner-employment-store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="">Judul Lowongan <span style="color:red">*</span></label>
                    <input type="text" class="form-control form-control-user @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Dicari supir handal" required autocomplete="title" autofocus>
                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-12 form-group">
                        <label for="">Pendapatan Perbulan (IDR) <span style="color:red">*</span></label>
                        <input type="number" min="1" class="form-control form-control-user @error('sallery') is-invalid @enderror" name="sallery" value="{{ old('sallery') }}" placeholder="10555001" required autocomplete="sallery" autofocus>
                        @error('sallery')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-4 col-md-12 form-group">
                        <label for="">Negara Tujuan <span style="color:red">*</span></label>
                        <select class="form-control form-control-user @error('country_employments_id') is-invalid @enderror" name="country_employments_id" value="{{ old('country_employments_id') }}" required autocomplete="country_employments_id" autofocus>
                            @foreach ($country as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        @error('country_employments_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-4 col-md-12 form-group">
                        <label for="">Total pekerja yang dibutuhkan <span style="color:red">*</span></label>
                        <input type="number" min="1" class="form-control form-control-user @error('needed') is-invalid @enderror" name="needed" value="{{ old('needed') }}" required autocomplete="needed" placeholder="100" autofocus>
                        @error('needed')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Bidang Pekerjaan <span style="color:red">*</span></label>
                    <select class="form-control form-control-user @error('sector_employments_id') is-invalid @enderror" name="sector_employments_id" value="{{ old('sector_employments_id') }}" required autocomplete="sector_employments_id" autofocus>
                        @foreach ($sector as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    @error('sector_employments_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Deskripsi <span style="color:red">*</span> </label>
                    <textarea name="description" cols="30" rows="5" class="form-control form-control-user @error('description') is-invalid @enderror" placeholder="Ex. Dicari supir yang sangat mahir" required autocomplete="description" autofocus>{{ old('description') }}</textarea>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Batas Pendaftaraan <span style="color:red">*</span></label>
                    <input type="date" class="form-control form-control-user @error('end_date') is-invalid @enderror" name="end_date" value="{{ old('end_date') }}" required autocomplete="end_date" autofocus>
                    @error('end_date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Banner Lowongan/ Cover</label>
                    <input type="file" class="form-control form-control-user @error('cover') is-invalid @enderror" name="cover" value="{{ old('cover') }}" autocomplete="cover" autofocus accept="image/x-png,image/gif,image/jpeg" />
                    @error('cover')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Buat Lowongan</button>
                </div>
            </form>
        </div>
    </div>

@endsection
