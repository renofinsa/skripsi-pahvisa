<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('partner')}}">
          <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-star"></i>
          </div>
          <div class="sidebar-brand-text mx-3">PPTKIS</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
          <li class="nav-item {{ (request()->is('partner')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('partner')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Beranda</span></a>
          </li>

          <!-- Divider -->
          <hr class="sidebar-divider">

          <!-- Heading -->
          <div class="sidebar-heading">
              Feature
          </div>

          <li class="nav-item {{ (request()->is('partner/users')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('partner-user')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Data CPMI</span></a>
          </li>

          <li class="nav-item {{ (request()->is('partner/employment')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('partner-employment')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Data Lowongan</span></a>
          </li>

          <li class="nav-item {{ (request()->is('partner/training')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('partner-training')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Data Pelatihan</span></a>
          </li>

          <li class="nav-item {{ (request()->is('partner/departure')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('partner-departure')}}">
              <i class="fas fa-fw fa-star"></i>
              <span>Data Keberangkatan</span></a>
          </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

      </ul>
