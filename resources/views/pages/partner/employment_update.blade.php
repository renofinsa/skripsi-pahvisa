@extends('layouts.app')

@section('title','Partner - Ubah Lowongan')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('partner')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('partner-employment')}}">Data Lowongan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{$data->title}}</li>
        </ol>
    </nav>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Ubah Data</h6>
        </div>
        <div class="card-body">
            <form action="{{route('partner-employment-update',$data->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group">
                    <img src="{{asset('storage/upload/partner_banner/'.$data->cover.' ')}}" class="card-img-top mb-2" style="height:420px; position: center" alt="...">
                    <label for="">Ubah Banner Lowongan/ Cover</label>
                    <input type="file" class="form-control form-control-user @error('cover') is-invalid @enderror" name="cover" value="{{ old('cover') }}" autocomplete="cover" autofocus accept="image/x-png,image/gif,image/jpeg" />
                    <input type="hidden" class="form-control form-control-user @error('old_cover') is-invalid @enderror" name="old_cover" value="{{ $data->cover }}" required autocomplete="old_cover" autofocus accept="image/x-png,image/gif,image/jpeg" />
                    @error('cover')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Judul Lowongan <span style="color:red">*</span></label>
                    <input type="text" class="form-control form-control-user @error('title') is-invalid @enderror" name="title" value="{{ $data->title }}" placeholder="Dicari supir handal" required autocomplete="title" autofocus>
                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-12 form-group">
                        <label for="">Pendapatan Perbulan (IDR) <span style="color:red">*</span></label>
                        <input type="number" min="1" class="form-control form-control-user @error('sallery') is-invalid @enderror" name="sallery" value="{{ $data->sallery }}" placeholder="10555001" required autocomplete="sallery" autofocus>
                        @error('sallery')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-4 col-md-12 form-group">
                        <label for="">Negara Tujuan <span style="color:red">*</span></label>
                        <select class="form-control form-control-user @error('country_employments_id') is-invalid @enderror" name="country_employments_id" value="{{ old('country_employments_id') }}" required autocomplete="country_employments_id" autofocus>
                            <option value="{{$data->country_employments_id}}">{{$data->country->name}}</option>
                            @foreach ($country as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        @error('country_employments_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-lg-4 col-md-12 form-group">
                        <label for="">Total pekerja yang dibutuhkan <span style="color:red">*</span></label>
                        <input type="number" min="1" class="form-control form-control-user @error('needed') is-invalid @enderror" name="needed" value="{{ $data->needed }}" required autocomplete="needed" placeholder="100" autofocus>
                        @error('needed')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Bidang Pekerjaan <span style="color:red">*</span></label>
                    <select class="form-control form-control-user @error('sector_employments_id') is-invalid @enderror" name="sector_employments_id" value="{{ old('sector_employments_id') }}" required autocomplete="sector_employments_id" autofocus>
                        <option value="{{$data->sector_employments_id}}">{{$data->sector->name}}</option>
                        @foreach ($sector as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    @error('sector_employments_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Deskripsi <span style="color:red">*</span> </label>
                    <textarea name="description" cols="30" rows="5" class="form-control form-control-user @error('description') is-invalid @enderror" placeholder="Ex. Dicari supir yang sangat mahir" required autocomplete="description" autofocus>{{ $data->description }}</textarea>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Batas Pendaftaraan <span style="color:red">*</span></label>
                    <input type="date" class="form-control form-control-user @error('end_date') is-invalid @enderror" name="end_date" value="{{date('Y-m-d', strtotime($data->end_date))}}" required autocomplete="end_date" autofocus>
                    @error('end_date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <p style="color:red">Catatan: Data yang diubah akan diproses kembali oleh APJATI hingga disetujui.</p>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Simpan</button>
                </div>
            </form>
        </div>
    </div>

@endsection
