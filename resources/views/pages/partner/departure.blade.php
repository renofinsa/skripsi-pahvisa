@extends('layouts.app')

@section('title','PPTKIS - CPMI')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Keberangkatan</li>
        </ol>
    </nav>

    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}

    <div class="card shadow mb-4">
        <div class="card-header py-3">

        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Nama Lengkap</th>
                  <th>Nomer Telepon</th>
                  <th>Judul Lowongan</th>
                  <th>Tanggal Pelatihan</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th width="10">No</th>
                  <th>Nama Lengkap</th>
                  <th>Nomer Telepon</th>
                  <th>Judul Lowongan</th>
                  <th>Tanggal Pelatihan</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item->user->profile->real_name}}</td>
                  <td>{{$item->user->profile->phone_number}}</td>
                  <td>{{$item->employment->partner->name}}</td>
                  <td>{{date('d M Y', strtotime($item->employment->end_date))}}</td>
                </tr>


                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection
