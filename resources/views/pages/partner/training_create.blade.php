@extends('layouts.app')

@section('title','Admin - Beranda')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('partner')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('partner-employment')}}">Data Lowongan</a></li>
            <li class="breadcrumb-item"><a href="{{route('partner-employment-show',$data->id)}}">Data Lowongan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Data - {{$data->title}}</li>
        </ol>
    </nav>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
        </div>
        <div class="card-body">
            <form action="{{route('partner-training-store')}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="id" value="{{$data->id}}">
                <div class="form-group">
                    <label for="">Judul Lowongan <span style="color:red">*</span></label>
                    <input disabled type="text" class="form-control form-control-user @error('title') is-invalid @enderror" name="title" value="{{ $data->title }}" placeholder="Dicari supir handal" required autocomplete="title" autofocus>
                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Terakhir daftar <span style="color:red">*</span></label>
                    <input  type="date" class="form-control form-control-user @error('last_join') is-invalid @enderror" name="last_join" value="{{old('last_join')}}" required autocomplete="last_join" autofocus>
                    @error('last_join')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Deskripsi pelatihan<span style="color:red">*</span> </label>
                    <textarea  name="description" cols="30" rows="10" class="form-control form-control-user @error('description') is-invalid @enderror" placeholder="Ex. Dicari supir yang sangat mahir" required autocomplete="description" autofocus>{{ old('description') }}</textarea>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Buat Pelatihan</button>
                </div>
            </form>
        </div>
    </div>

@endsection
