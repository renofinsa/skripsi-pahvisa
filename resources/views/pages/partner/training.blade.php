@extends('layouts.app')

@section('title','PPTKIS - CPMI')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('partner')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Jadwal Pelatihan</li>
        </ol>
    </nav>

    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}

    <div class="card shadow mb-4">
        <div class="card-header py-3">

        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Judul Lowongan</th>
                  <th>Tanggal Akhir Pendaftaraan</th>
                  <th>Total Peserta</th>
                  <th width="100">Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th width="10">No</th>
                  <th>Judul Lowongan</th>
                  <th>Tanggal Akhir Pendaftaraan</th>
                  <th>Total Peserta</th>
                  <th width="50">Action</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item->title}}</td>
                  <td>{{date('d M Y', strtotime($item->end_date))}}</td>
                  <td>
                      @if(count($item->register) < 1)
                        Belum ada peserta
                      @else
                      {{count($item->register)}} Peserta
                      @endif
                  </td>
                  <td>
                      <a class="btn btn-primary" href="{{route('partner-employment-show',$item->id)}}"> <i class="fas fa-eye"></i> </a>
                      <a class="btn btn-success" href="{{route('partner-participant',$item->id)}}"> <i class="fas fa-users"></i> </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection
