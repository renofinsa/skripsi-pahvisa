@extends('layouts.app')

@section('title','Admin - Dashboard')

@section('sidebar')
    @include('pages.user.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('user')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Lowongan - {{$data->title}}</li>
        </ol>
    </nav>

    {{-- alert --}}
    @include('partials.alert')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                    </div>
                </div>
                <div class="col-lg-3">
                    <form action="{{route('user-employment-save')}}" method="post">
                        @csrf
                        <input type="hidden" value="{{$data->id}}" name="employment_id">
                        <button style="margin: 2px" type="submit" class="btn btn-danger form-control"> <i class="fas fa-heart"></i> Simpan Lowongan</button>
                    </form>
                </div>
                <div class="col-lg-3">
                    <form action="{{route('user-departure-store')}}" method="post">
                        @csrf
                        <input type="hidden" value="{{$data->id}}" name="employment_id">
                        <button style="margin: 2px" type="submit" class="btn btn-success form-control"> <i class="fas fa-star"></i> Kirim Lowongan</button>
                    </form>
                </div>
            </div>
        </div>

        <img src="{{asset('storage/upload/partner_banner/'.$data->cover.' ')}}" class="card-img-top" style="height:420px; position: center" alt="...">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <h5 class="card-title">{{$data->title}}</h5>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <p style="text-align:right">Pendaftaran Akhir: <u>{{date('d M Y', strtotime($data->end_date))}} </u> </p>
                </div>
            </div>
            <ul>
                <li>Negara Tujuan: <b>{{$data->country->name}}</b> </li>
                <li>Sebagai: <b>{{$data->sector->name}}</b> </li>
                <li>Gaji/bulan: <b>IDR {{number_format($data->sallery,0,',','.')}}</b> </li>
                <li>Total pekerja yang dibutuhkan: <b>{{$data->needed}}</b> </li>
            </ul>
            <p class="card-text">{{$data->description}}</p>
            <hr>
            <h5 class="card-title" style="text-align:center"> <b>-- Penyelenggara --</b> </h5>
            <div class="row">
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <img style="width:150px" src="{{asset('/storage/upload/partner_logo/'.$data->partner->logo.'')}}" alt="">
                </div>
                <div class="col-lg-10 col-md-6 col-sm-12">
                    <h3>{{$data->partner->name}}
                        @for ($i = 0; $i < $data->partner->ratting; $i++)
                            <span class="fa fa-star checked"></span>
                        @endfor
                    </h3>
                    <p>
                        Alamat :{{ $data->partner->address }} <br>
                        PIC :{{ $data->partner->pic }} <br>
                        Nomer Telepon :{{ $data->partner->phone_number }} <br>
                        Email :{{ $data->partner->user->email }}
                    </p>
                </div>
            </div>

        </div>

    </div>

@endsection
