@extends('layouts.app')

@section('title','Admin - Dashboard')

@section('sidebar')
    @include('pages.user.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('user')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Lowongan Tersimpan</li>

        </ol>
    </nav>

    @include('partials.alert')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Lowongan Tersimpan</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                    <th width="10">No</th>
                    <th>Judul Lowongan</th>
                    <th>PPTKIS</th>
                    <th>Tanggal Kirim</th>
                    <th style="width:80px">Aksi</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                    <th width="10">No</th>
                    <th>Judul Lowongan</th>
                    <th>PPTKIS</th>
                    <th>Tanggal Kirim</th>
                    <th style="width:80px">Aksi</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php $no = 0 ?>
                    @foreach ($data as $item)
                    <?php $no++ ?>
                    <tr>
                    <td>{{$no}}</td>
                    <td>{{$item->employment->title}}</td>
                    <td>{{$item->partner->name}}</td>
                    <td>{{date('d M Y', strtotime($item->created_at))}}</td>
                    <td class="row">
                       <div class="col-md-6">
                           <form action="{{route('user-employment-destroy',$item->id)}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger" type="submit"> <i class="fas fa-trash"></i> </button>
                            </form>
                        </div>
                        <div class="col-md-6">
                                <a class="btn btn-primary" href="{{route('user-employment-show',$item->id)}}"> <i class="fas fa-info-circle"></i> </a>

                       </div>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
