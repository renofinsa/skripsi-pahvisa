@extends('layouts.app')

@section('title','User - Testimonial')

@section('sidebar')
    @include('pages.user.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('user')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Testimonial</li>
        </ol>
    </nav>

    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}

    <div class="card shadow mb-4">
        <div class="card-header py-3">

        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Nama Lengkap</th>
                  <th>Nomer Telepon</th>
                  <th>Judul Lowongan</th>
                  <th>PPTKIS</th>
                  <th>Disetujui</th>
                  <th width="30">Aksi</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th width="10">No</th>
                  <th>Nama Lengkap</th>
                  <th>Nomer Telepon</th>
                  <th>Judul Lowongan</th>
                  <th>PPTKIS</th>
                  <th>Disetujui</th>
                  <th width="30">Aksi</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item->user->profile->real_name}}</td>
                  <td>{{$item->user->profile->phone_number}}</td>
                  <td>{{$item->schedule->employment->title}}</td>
                  <td>{{$item->schedule->partner->name}}</td>
                  <td style="text-transform: capitalize">{{$item->is_approv}}</td>
                  <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->id}}">
                        <i class="fas fa-cog"></i>
                        </button>
                  </td>
                </tr>

                {{-- setup status dari peserta --}}
                    <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Penilaian</label> <br>
                                        @for ($i = 0; $i < $item->value; $i++)
                                            <span class="fa fa-star checked"></span>
                                        @endfor

                                    </div>
                                    <div class="form-group">
                                        <label for="">Testimonial</label>
                                        <p>{{$item->note}}</p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {{-- setup status dari peserta --}}


                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection
