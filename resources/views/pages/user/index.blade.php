@extends('layouts.app')

@section('title','Admin - Dashboard')

@section('sidebar')
    @include('pages.user.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Beranda</li>
        </ol>
    </nav>

    <div class="row">
        <form action="{{route('user')}}" method="get" class="form-inline mt-3 ml-3" style="">
            <input name="search" class="form-control mr-sm-2" style="width:420px;" type="search" placeholder="Judul pekerjaan" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
        </form>
    </div>

    <div class="row mt-3">
        @if(count($data) > 0)
        @foreach ($data as $item)
            <div class="col-md-4 col-sm-6 col-lg-4">
                <div class="card">
                    <img src="{{asset('storage/upload/partner_banner/'.$item->cover.' ')}}" class="card-img-top" style="height:200px; position: center" alt="...">
                    <div class="card-body">
                        <a href="{{route('user-employment-show',$item->id)}}"><h5 class="card-title">{{$item->title}}</h5></a>
                        <p class="card-text">{!! $item->sector->name  !!} - {!! $item->country->name  !!}</p>
                        <p class="card-text">{!! substr($item->description, 0,  80)  !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
        @else
            <h4>Data tidak ditemukan. Coba cari lagi!</h4>
        @endif
    </div>
    <div class="row justify-content-center mt-5">
        {{ $data->links() }}
    </div>


@endsection
