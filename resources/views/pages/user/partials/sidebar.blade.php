<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('user')}}">
          <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-star"></i>
          </div>
          <div class="sidebar-brand-text mx-3">{{Auth::user()->profile->status}}</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
          <li class="nav-item {{ (request()->is('user')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('user')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Beranda</span></a>
          </li>

          <!-- Divider -->
          <hr class="sidebar-divider">

          <!-- Heading -->
          <div class="sidebar-heading">
              Feature
          </div>

          <li class="nav-item {{ (request()->is('user/employment/save')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('user-employment')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Lowongan Tersimpan</span></a>
          </li>

          <li class="nav-item {{ (request()->is('user/departure')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('user-departure')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Lowongan Terkirim</span></a>
          </li>

          <li class="nav-item {{ (request()->is('user/testimonial')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('user-testimonial')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Testimonial</span></a>
          </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

      </ul>
