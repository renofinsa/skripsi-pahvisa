@extends('layouts.app')

@section('title','Admin - Dashboard')

@section('sidebar')
    @include('pages.user.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('user')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Lowongan Terkirim</li>

        </ol>
    </nav>

    @include('partials.alert')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Lowongan Terkirim</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                    <th width="10">No</th>
                    <th>Judul Lowongan</th>
                    <th>PPTKIS</th>
                    <th>Tanggal Kirim</th>
                    <th>Status</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                    <th width="10">No</th>
                    <th>Judul Lowongan</th>
                    <th>PPTKIS</th>
                    <th>Tanggal Kirim</th>
                    <th>Status</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php $no = 0 ?>
                    @foreach ($data as $item)
                    <?php $no++ ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$item->employment->title}}</td>
                        <td>{{$item->employment->partner->name}}</td>
                        <td>{{date('d M Y', strtotime($item->created_at))}}</td>
                        <td>
                            @if ($item->status == 'Bekerja' )
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#testimonial{{$item->id}}">
                                    Bekerja
                                </button>
                            @else
                                <span style="text-transform: capitalize">{{$item->status}}</span>
                            @endif

                        <!-- Testimonial Modal -->
                        <div class="modal fade" id="testimonial{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Berikan pendapat dan penilaian kamu tentang pelayanan pelatihan dengan judul <b>{{$item->id}}</b> dan penyelenggara  <b>{{$item->id}}</b>.</p>
                                    <form action="{{route('user-sent_ratting')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="employment_id" value="{{$item->id}}">
                                        <input type="hidden" name="partner_id" value="{{$item->id}}">
                                        <div class="form-group">
                                            <label for="">Ratting</label>
                                            <select name="value" id="" class="form-control">
                                                <option value="5">5</option>
                                                <option value="4">4</option>
                                                <option value="3">3</option>
                                                <option value="2">2</option>
                                                <option value="1">1</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Masukan / Komentar tentang pelathian</label>
                                            <textarea name="note" id="" cols="30" rows="10" class="form-control" placeholder="Ex. Pelatihannya sangat memuaskan"> {{old('note')}} </textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary form-control">Kirim Testimonial</button>
                                        </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
