<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>

    <style>
        #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #4CAF50;
          color: white;
        }
    </style>
</head>
<body>

	<div class="container">
		<center>
			<h4>
                LAPORAN PENEMPATAN <br>
                PEKERJA MIGRAN INDONESIA <br>
                PERIODE {{date('d M Y', strtotime($start))}} - {{date('d M Y', strtotime($end))}}
            </h4>
		</center>
		<br/>
		<table id="customers">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama PPTKIS</th>
					<th>Ratting</th>
					<th>Jumlah Lowongan</th>
					<th>Jumlah PMI</th>
				</tr>
			</thead>
			<tbody>
				@php $i=1 @endphp
				@php $total_emp = -1 @endphp
				@foreach($data as $item)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{$item->name}}</td>
                    <td>{{$item->ratting}}</td>
                    <td>{{$item->total_employment}} Lowongan</td>
                    <td>{{$item->total_pmi}} PMI</td>
                    <td style="display: none">{{$total_emp += $item->total_employment}}</td>
				</tr>
				@endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3"> <b style="float:right">Total</b> </td>
                    <td>{{$total_employment[0]->total_employment}} Lowongan</td>
                    <td>{{$total_pmi[0]->total_pmi}} PMI</td>
                </tr>
            </tfoot>
		</table>

	</div>

</body>
</html>
