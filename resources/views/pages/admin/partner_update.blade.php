@extends('layouts.app')

@section('title','Admin - Beranda')

@section('sidebar')
    @include('pages.admin.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin-partner')}}">PPTKIS</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{$data->name}} </li>
        </ol>
    </nav>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Ubah Data</h6>
        </div>
        <div class="card-body">
            <form action="{{route('admin-partner-update',$data->id)}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group">
                    <label for="">Email <span style="color:red">*</span></label>
                    <input disabled type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ $data->user->email }}" placeholder="Enter Email Address" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <hr>
                <div class="form-group">
                    <label for="">Nama Perusahaan <span style="color:red">*</span></label>
                    <input type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" value="{{ $data->name }}" placeholder="PT Kini Kreatif" required autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Nama Penanggung Jawab (PIC) <span style="color:red">*</span></label>
                    <input type="text" class="form-control form-control-user @error('pic') is-invalid @enderror" name="pic" value="{{ $data->pic }}" placeholder="Hasyim Adnan" autocomplete="pic" autofocus>
                    @error('pic')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Nomer Telepon <span style="color:red">*</span></label>
                    <input type="number" min="1" class="form-control form-control-user @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ $data->phone_number }}" placeholder="0211231232" required autocomplete="phone_number" autofocus>
                    @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Alamat Perusahaan <span style="color:red">*</span> </label>
                    <textarea name="address" cols="30" rows="5" class="form-control form-control-user @error('address') is-invalid @enderror" placeholder="Jl Permai Raya no. 21" autocomplete="address" autofocus>{{ $data->address }}</textarea>
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Negara <span style="color:red">*</span> </label>
                    <select name="country_employments_id" class="form-control form-control-user @error('country_employments_id') is-invalid @enderror" placeholder="Jl Permai Raya no. 21" autocomplete="country_employments_id" autofocus>
                        <option value="{{$data->country->id}}"> {{$data->country->name}} </option>
                        @foreach ($country as $item)
                            <option value="{{$item->id}}"> {{$item->name}} </option>
                        @endforeach
                    </select>
                    @error('country_employments_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Ubah</button>
                </div>
            </form>
        </div>
    </div>

@endsection
