@extends('layouts.app')

@section('title','PPTKIS - CPMI')

@section('sidebar')
    @include('pages.admin.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Jadwal Keberangkatan</li>
        </ol>
    </nav>

    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}

    <div class="card shadow mb-4">
        <div class="card-header py-3">

        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Nama Lengkap</th>
                  <th>Nomer Telepon</th>
                  <th>Judul Lowongan</th>
                  <th>PPTKIS</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th width="10">No</th>
                  <th>Nama Lengkap</th>
                  <th>Nomer Telepon</th>
                  <th>Judul Lowongan</th>
                  <th>PPTKIS</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                    <td>{{$no}}</td>
                    <td> <a href="{{route('admin-user-show', $item->user->profile->id)}}">{{$item->user->profile->real_name}}</a> </td>
                    <td>{{$item->user->profile->phone_number}}</td>
                    <td> <a href="{{route('admin-employment-show', $item->employment->id)}}">{{$item->employment->title}}</a> </td>
                    <td> <a href="{{route('admin-partner-show', $item->employment->partner->id)}}">{{$item->employment->partner->name}}</a> </td>
                </tr>



                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection
