@extends('layouts.app')

@section('title','Admin - Beranda')

@section('sidebar')
    @include('pages.admin.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Laporan</li>
        </ol>
    </nav>

    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <button type="button" style="float:right" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                <i class="fas fa-download"></i> Unduh Laporan
            </button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{route('report')}}" method="get">
                    <div class="modal-body">
                        <label for="">Tangal Laporan</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Dari</label>
                                    <input type="date" name="start" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Sampai</label>
                                    <input type="date" name="end" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Unduh Laporan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama PPTKIS</th>
                    <th>Ratting</th>
                    <th>Jumlah Lowongan</th>
                    <th>Jumlah PMI</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <td colspan="3"> <b style="float:right">Total</b> </td>
                    <td>{{$total_employment[0]->total_employment}} Lowongan</td>
                    <td>{{$total_pmi[0]->total_pmi}} PMI</td>
                    {{-- <td>{{$total_employment}}</td> --}}
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$item->name}}</td>
                    <td>
                        @if($item->ratting < 1 )
                        Belum ada
                        @else
                            @for ($i = 0; $i < $item->ratting; $i++)
                                <span class="fa fa-star checked"></span>
                            @endfor
                        @endif
                    </td>
                    <td>{{$item->total_employment}} Lowongan</td>
                    <td>{{$item->total_pmi}} PMI</td>
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection
