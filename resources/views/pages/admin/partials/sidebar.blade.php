<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('admin')}}">
          <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-star"></i>
          </div>
          <div class="sidebar-brand-text mx-3">APJATI</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
          <li class="nav-item {{ (request()->is('admin')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('admin')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Beranda</span></a>
          </li>

          <!-- Divider -->
          <hr class="sidebar-divider">

          <!-- Heading -->
          <div class="sidebar-heading">
              Feature
          </div>

          <li class="nav-item {{ (request()->is('admin/partner')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('admin-partner')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Data PPTKIS</span></a>
          </li>

          <li class="nav-item {{ (request()->is('admin/users')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('admin-user')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Data CPMI</span></a>
          </li>

          <li class="nav-item {{ (request()->is('admin/employment')) ? 'active' : '' }}">
            <a class="nav-link" href=" {{route('admin-employment')}} ">
              <i class="fas fa-fw fa-star"></i>
              <span>Data Lowongan</span></a>
          </li>

          <li class="nav-item {{ (request()->is('admin/rekrutment')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin-rekrutmen')}}">
              <i class="fas fa-fw fa-star"></i>
              <span>Progress Rekrutmen</span></a>
          </li>

          <li class="nav-item {{ (request()->is('admin/departure')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin-departure')}}">
              <i class="fas fa-fw fa-star"></i>
              <span>Data Keberangkatan</span></a>
          </li>

          <li class="nav-item {{ (request()->is('admin/testimonial')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin-testimonial')}}">
              <i class="fas fa-fw fa-star"></i>
              <span>Testimonial</span></a>
          </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

      </ul>
