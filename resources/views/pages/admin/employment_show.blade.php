@extends('layouts.app')

@section('title','Partner - Detail Employment')

@section('sidebar')
    @include('pages.partner.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('partner')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('partner-employment')}}">Data Lowongan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Data - {{$data->title}}</li>
        </ol>
    </nav>

    <div class="card shadow mb-4">
        <div class="card mt-3 mb-lg-5 mb-md-5 mb-sm-5">
            <img src="{{asset('storage/upload/partner_banner/'.$data->cover.' ')}}" class="card-img-top" style="height:420px; position: center" alt="...">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <h5 class="card-title">{{$data->title}}</h5>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <p style="text-align:right">Pendaftaran Akhir: <u>{{date('d M Y', strtotime($data->end_date))}} </u> </p>
                    </div>
                </div>
                <ul>
                    <li>Penyelenggara: <b>{{$data->partner->name}}</b> </li>
                    <li>Negara Tujuan: <b>{{$data->country->name}}</b> </li>
                    <li>Sebagai: <b>{{$data->sector->name}}</b> </li>
                    <li>Gaji/bulan: <b>IDR {{number_format($data->sallery,0,',','.')}}</b> </li>
                    <li>Total pekerja yang dibutuhkan: <b>{{$data->needed}}</b> </li>
                </ul>
                <p class="card-text">{{$data->description}}</p>
                <hr>
                <h5 class="card-title">Tanggapi Lowongan</h5>
                <form action="{{route('admin-employment-update', $data->id)}}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="row form-group m-2">
                        <div class="form-check col-md-6">
                            <input class="form-check-input" type="radio" name="is_publish" id="is_publish" value="disetujui" checked>
                            <label class="form-check-label" for="is_publish">
                                Disetujui
                            </label>
                        </div>
                        <div class="form-check col-md-6">
                            <input class="form-check-input" type="radio" name="is_publish" id="is_publish" value="ditolak">
                            <label class="form-check-label" for="is_publish">
                                Ditolak
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary form-control">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
