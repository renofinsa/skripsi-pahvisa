@extends('layouts.app')

@section('title','Admin - Beranda')

@section('sidebar')
    @include('pages.admin.partials.sidebar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">PPTKIS</li>
        </ol>
    </nav>

    {{-- include alert --}}
    @include('partials.alert')
    {{-- include alert --}}

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{route('admin-partner-create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" style="float:right">
                <i class="fas fa-plus fa-sm text-white-50"></i> Tambah PPTKIS
            </a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Nama PT</th>
                  <th>Nama PIC</th>
                  <th>Nomer Telepon</th>
                  <th>Ratting</th>
                  <th width="50">Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th width="10">No</th>
                  <th>Nama PT</th>
                  <th>Nama PIC</th>
                  <th>Nomer Telepon</th>
                  <th>Ratting</th>
                  <th width="50">Action</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no = 0 ?>
                @foreach ($data as $item)
                <?php $no++ ?>
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item->name}}</td>
                  <td>{{$item->pic}}</td>
                  <td>{{$item->phone_number}}</td>
                  <td>
                    @for ($i = 0; $i < $item->ratting; $i++)
                        <span class="fa fa-star checked"></span>
                    @endfor
                  </td>
                  <td>
                      <a class="btn btn-success" href="{{route('admin-partner-show',$item->id)}}"> <i class="fas fa-pencil-alt"></i> </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection
