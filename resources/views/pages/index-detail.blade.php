@extends('layouts.public')

@section('title', 'Pahvisa')

@section('content')

<div class="container">
    <div class="card mt-3 mb-lg-5 mb-md-5 mb-sm-5">
        <img src="{{asset('storage/upload/partner_banner/'.$data->cover.' ')}}" class="card-img-top" style="height:420px; position: center" alt="...">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <h5 class="card-title">{{$data->title}}</h5>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <p style="text-align:right">Pendaftaran Akhir: <u>{{date('d M Y', strtotime($data->end_date))}} </u> </p>
                </div>
            </div>
            <ul>
                <li>Penyelenggara: <b>{{$data->partner->name}}</b> </li>
                <li>Negara Tujuan: <b>{{$data->country->name}}</b> </li>
                <li>Sebagai: <b>{{$data->sector->name}}</b> </li>
                <li>Gaji/bulan: <b>IDR {{number_format($data->sallery,0,',','.')}}</b> </li>
                <li>Total pekerja yang dibutuhkan: <b>{{$data->needed}}</b> </li>
            </ul>
            <p class="card-text">{{$data->description}}</p>
            <a style="float:right" href="{{route('login')}}" class="btn btn-primary">Kirim Lamaran</a>
        </div>
    </div>
</div>
@endsection
