@extends('layouts.public')

@section('title', 'Pahvisa')

@section('content')

<div class="container">
    <div class="row">
        <form action="{{route('index')}}" method="get" class="form-inline mt-3 ml-3" style="">
            <input name="search" class="form-control mr-sm-2" style="width:420px;" type="search" placeholder="Judul pekerjaan" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
        </form>
    </div>
    <!-- Outer Row -->
    <div class="row mt-3">
            @if(count($data) > 0)
            @foreach ($data as $item)
                <div class="col-md-4 col-sm-6 col-lg-4">
                    <div class="card">
                        <img src="{{asset('storage/upload/partner_banner/'.$item->cover.' ')}}" class="card-img-top" style="height:200px; position: center" alt="...">
                        <div class="card-body">
                            <a href="{{route('index-detail',$item->id)}}"><h5 class="card-title">{{$item->title}}</h5></a>
                            <p class="card-text">{!! $item->sector->name  !!} - {!! $item->country->name  !!}</p>
                            <p class="card-text">{!! substr($item->description, 0,  80)  !!}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            @else
                <h4>Data tidak ditemukan. Coba cari lagi!</h4>
            @endif
    </div>
    <div class="row justify-content-center mt-5">
        {{ $data->links() }}
    </div>
</div>

@endsection
