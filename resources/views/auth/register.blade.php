@extends('layouts.public')

@section('title', 'Pahvisa')

@section('content')

<div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

          <div class="col-xl-6 col-lg-6 col-md-6">

            <div class="card o-hidden border-0 shadow-lg my-5">
              <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="p-5">
                    <img class="mb-4" style="width:100%" src="img/logo.png" alt="">
                      <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Mendaftar Sebagai CPMI</h1>
                        <hr>
                      </div>
                      <form method="POST" action="{{ route('daftar') }}">
                        @csrf
                        <input type="hidden" name="role_id" value="2">
                        <div class="form-group">
                            <label for="">Email <span style="color:red">*</span> </label>
                            <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="nama@domain.com" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Kata Sandi <span style="color:red">*</span> </label>
                            <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" placeholder="Kata Sandi" required autocomplete="password" autofocus>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{-- biodata --}}
                        <hr>
                        <p style="text-align:center">Data Lengkap</p>
                        <div class="form-group">
                            <label for="">Nomer KTP <span style="color:red">*</span> </label>
                            <input type="number" min="1" class="form-control form-control-user @error('ktp') is-invalid @enderror" name="ktp" value="{{ old('ktp') }}" placeholder="1122334455" required autocomplete="ktp" autofocus>
                            @error('ktp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Nama Lengkap <span style="color:red">*</span> </label>
                            <input type="text" class="form-control form-control-user @error('real_name') is-invalid @enderror" name="real_name" value="{{ old('real_name') }}" placeholder="Hasyim Adnan" required autocomplete="real_name" autofocus>
                            @error('real_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Nomer Telepon <span style="color:red">*</span> </label>
                            <input type="number" min="1" class="form-control form-control-user @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" placeholder="082211223344" required autocomplete="phone_number" autofocus>
                            @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Alamat Sesuai KTP <span style="color:red">*</span> </label>
                            <textarea name="address" cols="30" rows="5" class="form-control form-control-user @error('address') is-invalid @enderror" placeholder="Jl Permai Raya no. 21" required autocomplete="address" autofocus>{{ old('address') }}</textarea>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Negara Asal <span style="color:red">*</span> </label>
                            <select name="country_employments_id" class="form-control form-control-user @error('country_employments_id') is-invalid @enderror" placeholder="Jl Permai Raya no. 21" required autocomplete="country_employments_id" autofocus>
                                @foreach ($country as $item)
                                    <option value="{{$item->id}}"> {{$item->name}} </option>
                                @endforeach
                            </select>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{-- education --}}
                        <hr>
                        <p style="text-align:center">Pendidikan Terakhir</p>
                        <div class="form-group">
                            <label for="">Sekolah / Lembaga</label>
                            <input type="text" class="form-control form-control-user @error('education_school') is-invalid @enderror" name="education_school" value="{{ old('education_school') }}" placeholder="Sekolah KiniKreatif"  autocomplete="education_school" autofocus>
                            @error('education_school')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6 col-sm-12">
                                <label for="">Tahun Masuk</label>
                                <input type="date" class="form-control form-control-user @error('education_start_year') is-invalid @enderror" name="education_start_year" value="{{ old('education_start_year') }}"  autocomplete="education_start_year" autofocus>
                                @error('education_start_year')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <label for="">Tahun Kelulusan</label>
                                <input type="date" class="form-control form-control-user @error('education_end_year') is-invalid @enderror" name="education_end_year" value="{{ old('education_end_year') }}"  autocomplete="education_end_year" autofocus>
                                @error('education_end_year')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <p style="text-align:center">Pekerjaan Terakhir</p>
                        <div class="form-group">
                            <label for="">Nama Perusahaan</label>
                            <input type="text" class="form-control form-control-user @error('experiences_company') is-invalid @enderror" name="experiences_company" value="{{ old('experiences_company') }}" placeholder="PT Kini Kreatif"  autocomplete="experiences_company" autofocus>
                            @error('experiences_company')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Posisi</label>
                            <input type="text" class="form-control form-control-user @error('experiences_position') is-invalid @enderror" name="experiences_position" value="{{ old('experiences_position') }}" placeholder="Juru Mudi / Pengemudi"  autocomplete="experiences_position" autofocus>
                            @error('experiences_position')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Bidang Pekerjaan</label>
                            <input type="text" class="form-control form-control-user @error('experiences_classification') is-invalid @enderror" name="experiences_classification" value="{{ old('experiences_classification') }}" placeholder="Perusahaan Tambang"  autocomplete="experiences_classification" autofocus>
                            @error('experiences_classification')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary btn-user btn-block">Daftar Sekarang</button>
                      </form>
                      <hr>
                      <div class="row">
                          <div class="col-6">
                              <p style="font-size:12px">Tanda (<span style="color:red">*</span>) adalah wajib.</p>
                          </div>
                          <div class="col-6">
                            <a href="{{route('login')}}" style="float:right">Sudah memiliki akun?</a>
                          </div>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>

      </div>

@endsection
