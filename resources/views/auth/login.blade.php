@extends('layouts.public')

@section('title', 'Pahvisa')

@section('content')

<div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center mt-3">
            @include('partials.alert')
        </div>
        <div class="row justify-content-center">

          <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="card o-hidden border-0 shadow-lg my-5">
              <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="p-5">
                    <img class="mb-4" style="width:100%" src="img/logo.png" alt="">
                      <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Masuk Aplikasi</h1>
                        <hr>
                      </div>
                      <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <input type="hidden" name="role_id" value="2">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Alamat Email" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Kata Sandi</label>
                            <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" placeholder="Password" required autocomplete="password" autofocus>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary btn-user btn-block">Masuk</button>
                      </form>
                      <hr>
                      <a href="{{route('register')}}" style="float:right">Belum memiliki akun? Daftar disini</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>

      </div>

@endsection
