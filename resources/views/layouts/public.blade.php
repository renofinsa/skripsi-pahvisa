<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/public.css') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" crossorigin="anonymous">

  </head>
  <body @yield('body-style')>
      @include('partials.public_navbar')
    @yield('content')
    @include('partials.footer')

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('vendor/bootstrap/js/slim.min.js') }}" ></script>
    <script src="{{ asset('vendor/bootstrap/js/popper.min.js') }}" ></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}" ></script>
  </body>
</html>
