<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::middleware('guest')->group(function () {
//     Route::get('/', 'DashboardController@index')->name('landing');
// });

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::get('/logout-200', '\App\Http\Controllers\Auth\LoginController@logout200')->name('logout200');
Route::get('/200', function () {
    return view('pages.200');
});
Route::middleware('guest')->group(function () {
    Auth::routes();
    Route::get('/', 'IndexController@index')->name('index');
    Route::get('/detail={id}', 'IndexController@show')->name('index-detail');
    Route::post('/daftar', 'IndexController@store')->name('daftar');

});


// admin middleware
Route::group(['middleware' => ['authadmin'],'prefix' => 'admin'],function (){
    // menu utama
    Route::get('/', 'Admin\IndexController@index')->name('admin');
    Route::get('/testimonial', 'Admin\IndexController@testimonial')->name('admin-testimonial');
    Route::patch('/testimonial/approv={id}', 'Admin\IndexController@testimonial_approv')->name('admin-testimonial-approv');
    // halaman PPTKIS / Partner
    Route::get('/partner', 'Admin\PartnerController@index')->name('admin-partner');
    Route::get('/partner/create', 'Admin\PartnerController@create')->name('admin-partner-create');
    Route::post('/partner/store', 'Admin\PartnerController@store')->name('admin-partner-store');
    Route::get('/partner/detail={id}', 'Admin\PartnerController@show')->name('admin-partner-show');
    Route::patch('/partner/update={id}', 'Admin\PartnerController@update')->name('admin-partner-update');

    // halaman CPMI / User level 3
    Route::get('/users', 'Admin\UserController@index')->name('admin-user');
    Route::get('/users/detail={id}', 'Admin\UserController@show')->name('admin-user-show');
    Route::patch('/users/update={id}', 'Admin\UserController@update')->name('admin-user-update');

    // halaman Lowongan / Employments
    Route::get('/employment', 'Admin\EmploymentController@index')->name('admin-employment');
    Route::get('/employment/detail={id}', 'Admin\EmploymentController@show')->name('admin-employment-show');
    Route::patch('/employment/update={id}', 'Admin\EmploymentController@update')->name('admin-employment-update');

    // halaman keberangkatan, view and update
    Route::get('/departure', 'Admin\DepartureController@index')->name('admin-departure');
    Route::patch('/departure/update={id}', 'Admin\DepartureController@update')->name('admin-departure-update');

    // halaman progress rekrutmen
    Route::get('/rekrutment', 'Admin\UserController@rekrutmen')->name('admin-rekrutmen');

    // Laporan
    Route::get('/download-report', 'Admin\UserController@report')->name('report');
});

Route::group(['middleware' => ['authpartner'],'prefix' => 'partner'],function (){
    // partner middleware
    Route::get('/', 'Partner\IndexController@index')->name('partner');

    // data keberangkatan order by partner_id
    Route::get('/departure', 'Partner\IndexController@departure')->name('partner-departure');

     // halaman CPMI / User level 3
     Route::get('/users', 'Partner\UserController@index')->name('partner-user');
     Route::get('/users/detail={id}', 'Partner\UserController@show')->name('partner-user-show');

     // halaman Lowongan / Employments
     Route::get('/employment', 'Partner\EmploymentController@index')->name('partner-employment');
     Route::get('/employment/detail={id}', 'Partner\EmploymentController@show')->name('partner-employment-show');
     Route::get('/employment/update={id}', 'Partner\EmploymentController@edit')->name('partner-employment-edit');
     Route::get('/employment/create', 'Partner\EmploymentController@create')->name('partner-employment-create');
     Route::post('/employment/store', 'Partner\EmploymentController@store')->name('partner-employment-store');
     Route::patch('/employment/update={id}', 'Partner\EmploymentController@update')->name('partner-employment-update');

     // peserta training
     Route::get('/departure', 'Partner\TrainingController@departure')->name('partner-departure');
     Route::get('/training/{id}/participant', 'Partner\TrainingController@participant')->name('partner-participant');
     Route::patch('/training/user={id}', 'Partner\TrainingController@participant_progress')->name('partner-participant-progress');

     // halaman training
     Route::get('/training', 'Partner\TrainingController@index')->name('partner-training');
     Route::get('/training/id={id}/create', 'Partner\TrainingController@create')->name('partner-training-create');
     Route::get('/training/show={id}', 'Partner\TrainingController@show')->name('partner-training-show');
     Route::post('/training/store', 'Partner\TrainingController@store')->name('partner-training-store');
     Route::patch('/training/update={id}', 'Partner\TrainingController@update')->name('partner-training-update');
     Route::delete('/training/delete={$id}', 'Partner\TrainingController@destroy')->name('partner-training-destroy');


});

Route::group(['middleware' => ['authuser'],'prefix' => 'user'],function (){
    // user middleware
    Route::get('/', 'User\IndexController@index')->name('user');

    // melihat seluruh data lowongan yang tersimpan
    Route::get('/employment/save', 'User\EmploymentController@index')->name('user-employment');
    // store data simpan lowongan
    Route::post('/employment/save/store', 'User\EmploymentController@store')->name('user-employment-save');
    // melihat detail lowongan, terdapat 2 button simpan lowongan dan mengirim lowongan
    Route::get('/employment/detail={id}', 'User\EmploymentController@show')->name('user-employment-show');
    Route::delete('/employment/destroy={id}', 'User\EmploymentController@destroy')->name('user-employment-destroy');

    // action mengirim lowongan
    Route::post('/departure/store', 'User\DepatureController@store')->name('user-departure-store');
    // melihat seluruh data pengiriman lowongan
    Route::get('/departure', 'User\DepatureController@index')->name('user-departure');
    // melihat melihat detail lowongan, terdapat button batal mengirim / hapus data
    Route::get('/departure/detail={id}', 'User\DepatureController@show')->name('user-departure-detail');
    // mengirim data ratting
    Route::post('/ratting/store', 'User\DepatureController@sent_ratting')->name('user-sent_ratting');
    Route::get('/testimonial', 'User\IndexController@testimonial')->name('user-testimonial');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
