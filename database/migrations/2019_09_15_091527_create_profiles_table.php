<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedSmallInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // real profile
            $table->string('real_name',50);
            $table->string('phone_number',13);
            $table->string('address',50)->nullable();
            $table->string('card_id',16)->nullable();
            $table->unsignedSmallInteger('country_employments_id')->nullable();
            $table->foreign('country_employments_id')->references('id')->on('country_employments')->onDelete('cascade');
            // documents
            $table->string('uploads_card_id',100)->nullable();
            $table->string('uploads_resume',100)->nullable();
            $table->string('uploads_passport',100)->nullable();
            // education
            $table->string('education_school_name',30)->nullable();
            $table->year('education_start_year')->nullable();
            $table->year('education_end_year')->nullable();
            // experiences
            $table->string('experiences_company',30)->nullable();
            $table->string('experiences_position',30)->nullable();
            $table->string('experiences_classification',30)->nullable();
            $table->enum('status',['CPMI','PMI']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
