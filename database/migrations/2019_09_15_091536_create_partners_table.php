<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedSmallInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('logo',100)->nullable();
            $table->string('name',50);
            $table->string('pic',20);
            $table->string('phone_number',13);
            $table->unsignedSmallInteger('country_employments_id')->nullable();
            $table->foreign('country_employments_id')->references('id')->on('country_employments')->onDelete('cascade');
            $table->string('address',50);
            $table->smallInteger('ratting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
