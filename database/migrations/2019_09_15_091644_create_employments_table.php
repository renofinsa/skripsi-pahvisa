<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employments', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedSmallInteger('partner_id');
            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('cascade');
            $table->string('title',100);
            $table->unsignedSmallInteger('sector_employments_id');
            $table->foreign('sector_employments_id')->references('id')->on('sector_employments')->onDelete('cascade');
            $table->text('description');
            $table->string('sallery',8);
            $table->unsignedSmallInteger('country_employments_id');
            $table->foreign('country_employments_id')->references('id')->on('country_employments')->onDelete('cascade');
            $table->datetime('end_date');
            $table->string('needed',3);
            $table->enum('is_publish',['menunggu','disetujui','ditolak']); // approv by admin
            $table->string('cover');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employments');
    }
}
