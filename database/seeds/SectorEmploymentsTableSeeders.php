<?php

use Illuminate\Database\Seeder;
use App\SectorEmployment;
class SectorEmploymentsTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SectorEmployment::create([
            'name'=>'Buruh Pabrik',
            'description'=>'Buruh Pabrik'
        ]);

        SectorEmployment::create([
            'name'=>'Asisten Rumah Tangga',
            'description'=>'Asisten Rumah Tangga'
        ]);

        SectorEmployment::create([
            'name'=>'Supir Pribadi',
            'description'=>'Supir Pribadi'
        ]);

        SectorEmployment::create([
            'name'=>'Penjaga Keamanan',
            'description'=>'Penjaga Keamanan'
        ]);

        SectorEmployment::create([
            'name'=>'Pemburu',
            'description'=>'Pemburu'
        ]);

        SectorEmployment::create([
            'name'=>'Pawang Hewan',
            'description'=>'Pawang Hewan'
        ]);

        SectorEmployment::create([
            'name'=>'Penjaga Toko',
            'description'=>'Penjaga Toko'
        ]);

        SectorEmployment::create([
            'name'=>'Pelayan Restoran',
            'description'=>'Pelayan Restoran'
        ]);

        SectorEmployment::create([
            'name'=>'Joki 3in1',
            'description'=>'Joki 3in1'
        ]);
    }
}
