<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeders::class);
        $this->call(CountryEmploymentsTableSeeders::class);
        $this->call(SectorEmploymentsTableSeeders::class);
        $this->call(UsersTableSeeders::class);
        $this->call(EmploymentSeedTables::class);
    }
}
