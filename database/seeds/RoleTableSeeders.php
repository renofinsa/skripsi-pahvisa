<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([ // 1
            'name'=>'Admin',
            'description'=>'As Admin'
        ]);
        Role::create([ // 2
            'name'=>'Partner',
            'description'=>'As Partner'
        ]);
        Role::create([ // 3
            'name'=>'User',
            'description'=>'As User'
        ]);
    }
}
