<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Partner;
use App\Profile;
class UsersTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // administrator
        $admin = new User();
        $admin->email = 'me@admin.com';
        $admin->password = bcrypt('admin');
        $admin->role_id = 1;
        $admin->is_approv = 'true';
        $admin->save();

        // partner
        $partner = new User();
        $partner->email = 'me@partner.com';
        $partner->password = bcrypt('admin');
        $partner->role_id = 2;
        $partner->is_approv = 'true';
        $partner->save();

        $datapartner = new Partner();
        $datapartner->ratting = 0;
        $datapartner->user_id = $partner->id;
        $datapartner->logo = 'partner_logo_default.png';
        $datapartner->name = 'PT Gile Ndro';
        $datapartner->pic = 'Indro';
        $datapartner->phone_number = '02194191972';
        $datapartner->country_employments_id = 1;
        $datapartner->address = 'jl pejaten village, no 21, jakarta selatan';
        $datapartner->save();

        // partner baru
        $partner2 = new User();
        $partner2->email = 'me@partner2.com';
        $partner2->password = bcrypt('admin');
        $partner2->role_id = 2;
        $partner2->is_approv = 'true';
        $partner2->save();

        $datapartner2 = new Partner();
        $datapartner2->ratting = 0;
        $datapartner2->user_id = $partner2->id;
        $datapartner2->logo = 'partner_logo_default.png';
        $datapartner2->name = 'PT Dono Makmur';
        $datapartner2->pic = 'Dono';
        $datapartner2->phone_number = '02194191972';
        $datapartner2->country_employments_id = 1;
        $datapartner2->address = 'jl Lontar timur, no 21, jakarta barat';
        $datapartner2->save();

        // user 1
        $user = new User();
        $user->email = 'me@user.com';
        $user->password = bcrypt('admin');
        $user->role_id = 3;
        $user->is_approv = 'false';
        $user->save();

        $dataprofile = new Profile();
        $dataprofile->user_id = $user->id;
        $dataprofile->real_name = 'Kasino';
        $dataprofile->phone_number = '082297770610';
        $dataprofile->country_employments_id = 1;
        $dataprofile->status = true;
        $dataprofile->save();

        // user 2
        $user2 = new User();
        $user2->email = 'me@user2.com';
        $user2->password = bcrypt('admin');
        $user2->role_id = 3;
        $user2->is_approv = 'false';
        $user2->save();

        $dataprofile2 = new Profile();
        $dataprofile2->user_id = $user2->id;
        $dataprofile2->real_name = 'Udin';
        $dataprofile2->phone_number = '082297770612';
        $dataprofile2->country_employments_id = 3;
        $dataprofile2->status = true;
        $dataprofile2->save();

        // user 3
        $user3 = new User();
        $user3->email = 'me@user3.com';
        $user3->password = bcrypt('admin');
        $user3->role_id = 3;
        $user3->is_approv = 'false';
        $user3->save();

        $dataprofile3 = new Profile();
        $dataprofile3->user_id = $user3->id;
        $dataprofile3->real_name = 'Maemunah';
        $dataprofile3->phone_number = '082297770611';
        $dataprofile3->country_employments_id = 4;
        $dataprofile3->status = true;
        $dataprofile3->save();

    }
}
