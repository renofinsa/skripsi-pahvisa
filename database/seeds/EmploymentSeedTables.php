<?php

use Illuminate\Database\Seeder;
use App\Employment;
class EmploymentSeedTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data lowongan aktif
        Employment::create([
            'partner_id'            => 1,
            'title'                 => 'Lowongan Pekerjaan A',
            'sector_employments_id' => 3,
            'description'           => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae animi ex, ipsam cum a harum dolorem quidem provident tempora consequatur expedita itaque. A sunt autem aut porro doloribus magni quibusdam.',
            'sallery'               => 14000000,
            'country_employments_id'=> 5,
            'end_date'              => 20191121,
            'needed'                => 100,
            'is_publish'            => 2,
            'cover'                 => 'default-banner.png'
        ]);

        // data lowongan aktif
        Employment::create([
            'partner_id'            => 1,
            'title'                 => 'Lowongan Kerja C',
            'sector_employments_id' => 7,
            'description'           => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae animi ex, ipsam cum a harum dolorem quidem provident tempora consequatur expedita itaque. A sunt autem aut porro doloribus magni quibusdam.',
            'sallery'               => 16600000,
            'country_employments_id'=> 7,
            'end_date'              => 20191121,
            'needed'                => 120,
            'is_publish'            => 2,
            'cover'                 => 'default-banner.png'
        ]);

        // data lowongan tidak aktif
        Employment::create([
            'partner_id'            => 1,
            'title'                 => 'Ada Pekerjaan B',
            'sector_employments_id' => 5,
            'description'           => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae animi ex, ipsam cum a harum dolorem quidem provident tempora consequatur expedita itaque. A sunt autem aut porro doloribus magni quibusdam.',
            'sallery'               => 15500000,
            'country_employments_id'=> 4,
            'end_date'              => 20191225,
            'needed'                => 150,
            'is_publish'            => 1,
            'cover'                 => 'default-banner.png'
        ]);

        // data lowongan aktif
        Employment::create([
            'partner_id'            => 2,
            'title'                 => 'Pekerjaan Baru',
            'sector_employments_id' => 5,
            'description'           => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae animi ex, ipsam cum a harum dolorem quidem provident tempora consequatur expedita itaque. A sunt autem aut porro doloribus magni quibusdam.',
            'sallery'               => 15500000,
            'country_employments_id'=> 4,
            'end_date'              => 20191229,
            'needed'                => 180,
            'is_publish'            => 2,
            'cover'                 => 'default-banner.png'
        ]);








    }
}
