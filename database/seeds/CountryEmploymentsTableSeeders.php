<?php

use Illuminate\Database\Seeder;
use App\CountryEmployment;
class CountryEmploymentsTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CountryEmployment::create([
            'name'=>'Indonesia',
            'description'=>'Indonesia'
        ]);

        CountryEmployment::create([
            'name'=>'Arab',
            'description'=>'Arab'
        ]);

        CountryEmployment::create([
            'name'=>'Taiwan',
            'description'=>'Taiwan'
        ]);

        CountryEmployment::create([
            'name'=>'Hongkong',
            'description'=>'Hongkong'
        ]);

        CountryEmployment::create([
            'name'=>'Malaysa',
            'description'=>'Malaysa'
        ]);

        CountryEmployment::create([
            'name'=>'Singapura',
            'description'=>'Singapura'
        ]);

        CountryEmployment::create([
            'name'=>'Philipina',
            'description'=>'Philipina'
        ]);

        CountryEmployment::create([
            'name'=>'Brunai',
            'description'=>'Brunai'
        ]);

        CountryEmployment::create([
            'name'=>'Dubai',
            'description'=>'Dubai'
        ]);

        CountryEmployment::create([
            'name'=>'Jepang',
            'description'=>'Jepang'
        ]);

        CountryEmployment::create([
            'name'=>'Korea',
            'description'=>'Korea'
        ]);

        CountryEmployment::create([
            'name'=>'Australia',
            'description'=>'Australia'
        ]);

        CountryEmployment::create([
            'name'=>'Amerika Serikat',
            'description'=>'Amerika Serikat'
        ]);
    }
}
