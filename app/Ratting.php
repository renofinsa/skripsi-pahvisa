<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ratting extends Model
{
    public function employment(){
        return $this->belongsTo(Employment::class,'employment_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
