<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveEmployment extends Model
{
    public function partner(){
        return $this->belongsTo(Partner::class,'id');
    }
    public function employment(){
        return $this->belongsTo(Employment::class,'employment_id');
    }
}
