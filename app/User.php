<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->belongsTo(Role::class,'role_id');
    }

    public function profile(){
        return $this->hasOne(Profile::class,'user_id');
    }

    public function profile2(){
        return $this->hasOne(Profile::class,'id');
    }

    public function partner(){
        return $this->hasOne(Partner::class,'id');
    }

    public function partner2(){
        return $this->hasOne(Partner::class,'user_id');
    }

    public function employment(){
        return $this->hasMany(Employment::class,'user_id');
    }



}
