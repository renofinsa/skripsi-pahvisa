<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{

    public function partner(){
        return $this->belongsTo(Partner::class,'partner_id');
    }

    public function sector(){
        return $this->belongsTo(SectorEmployment::class,'sector_employments_id');
    }

    public function country(){
        return $this->belongsTo(CountryEmployment::class,'country_employments_id');
    }

    public function register(){
        return $this->hasMany(RegisterEmployment::class,'id');
    }

    public function register2(){
        return $this->hasMany(RegisterEmployment::class,'employment_id');
    }

}
