<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    public function partner(){
        return $this->belongsTo(Partner::class,'partner_id');
    }
    public function employment(){
        return $this->belongsTo(Employment::class,'employment_id');
    }
    public function ratting(){
        return $this->hasMany(Ratting::class,'schedule_id');
    }
    public function register(){
        return $this->hasMany(RegisterEmployment::class,'schedule_id');
    }
}
