<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\SectorEmployment;
use App\CountryEmployment;
use App\Employment;
use App\Profile;
use App\User;
use App\Partner;
use App\SaveEmployment;
use App\RegisterEmployment;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employment = Employment::orderBy('created_at','desc')->limit(10)->get();
        $sector = SectorEmployment::orderBy('name','asc')->get();
        $country = CountryEmployment::orderBy('name','asc')->get();
        return view('user.index',compact('country','sector','employment'));
    }

    public function employment()
    {
        $sector = SectorEmployment::orderBy('name','asc')->get();
        $country = CountryEmployment::orderBy('name','asc')->get();
        $employment = Employment::orderBy('created_at','desc')->paginate(10);
        return view('user.employment',compact('employment','country','sector'));
    }

    public function employmentDetail($id)
    {
        // check penyimpanan lowongan
        $check = SaveEmployment::where([['user_id',Auth::user()->id],['employment_id',$id]])->get();
        $dd = [];
        if(count($check) > 0){
            $dd = [
                $method = '<form action="/user/employment/delete/id='.$id.'" method="post">',
                $button = '<button type="submit" class="btn btn-block btn-danger btn-md"><span class="icon-heart-o mr-2 text-white"></span>Hapus</button>',
                $method1 = '<input type="hidden" name="_method" value="DELETE">
                            ',
            ];
        }else{
            $dd = [
                $method = '<form action="/user/employment/save" method="post">',
                $button = '<button type="submit" class="btn btn-block btn-info btn-md"><span class="icon-heart-o mr-2 text-danger"></span>Simpan</button>',
                $method1 = '<input type="hidden" name="_method" value="POST">
                            <input type="hidden" value="'.$id.'" name="employment_id">
                            ',
            ];
        };

        // check pendaftar
        $check_register = RegisterEmployment::where([['user_id',Auth::user()->id],['employment_id',$id]])->get();
        $ddd = [];
        if(count($check_register) > 0){
            $ddd = [
                $method = '',
                $button = '<button class="btn btn-block btn-success btn-md" disabled="disabled" style="color:#fff">Terkirim</button>',
            ];
        }else{
            $ddd = [
                $method = ' <form action="/user/apply" method="post">',
                $button = ' <input type="hidden" value="'.$id.'" name="employment_id">
                            <button type="submit" class="btn btn-block btn-success btn-md" style="color:#fff">Mengajukan</button>
                            </form>
                            ',
            ];
        };
        // dd($ddd);


        $data = Employment::find($id);
        return view('user.employment-detail',compact('data','dd','ddd',));
    }

    public function employmentApply()
    {

        // return view('user.index');
    }

    public function doApply(Request $request)
    {
        $check = Profile::where('user_id', Auth::user()->id)->get();
        // dd($check[0]->status);

        if($check[0]->status === 'false'){
            return redirect('/user/setting')->with('alert','Data diri kamu belum lengkap, silakan lengkapi terlebih dahulu.');
        }else{
            $data = new RegisterEmployment();
            $data->user_id = Auth::user()->id;
            $data->employment_id = $request->get('employment_id');
            $data->status = 1;
            $data->save();
            return redirect()->back()->with('alert','Berhasil mendaftar.');
        }
    }

    public function setting()
    {
        $data = User::find(Auth::user()->id);
        $country = CountryEmployment::orderBy('name','asc')->get();
        $jumlahlowongan = RegisterEmployment::where('user_id',Auth::user()->id)->count();
        $simpanlowongan = SaveEmployment::where('user_id',Auth::user()->id)->count();
        return view('user.setting',compact('data','country','jumlahlowongan','simpanlowongan'));
    }

    public function employmentSave(Request $request)
    {
        $data = new SaveEmployment();
        $data->user_id = Auth::user()->id;
        $data->employment_id = $request->get('employment_id');
        $data->save();
        return redirect()->back()->with('alert','Lowongan berhasil disimpan.');
    }

    public function employmentDelete($id)
    {
        $check = SaveEmployment::where([['user_id',Auth::user()->id],['employment_id',$id]])->get();
        $data = SaveEmployment::find($check[0]->id);
        $data->delete();
        // dd($data[0]->id);
        return redirect()->back()->with('alertWarning','Lowongan berhasil dihapus.');
    }
    public function Apply(Request $request, $id)
    {
        // return view('user.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function partner(Request $request)
    {
        $data = Partner::where('name','like','%'.$request->get('key').'%')->orderBy('name','asc')->paginate(10);
        return view('user.partner',compact('data'));
    }

    public function partnerDetail($id)
    {
        $data = Partner::find($id);
        $employment = Employment::where('user_id',$data->user->id)->orderBy('created_at','desc')->paginate('5');
        // dd($employment);
        return view('user.partner-detail', compact('data','employment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchEmployment(Request $request)
    {
        $sector = SectorEmployment::orderBy('name','asc')->get();
        $country = CountryEmployment::orderBy('name','asc')->get();
        // kata kunci
        $get_key = $request->get('key');
        $get_sector = $request->get('sector');
        $get_country = $request->get('country');

        $employment = Employment::where('title','like','%'.$get_key.'%')->where('sector_employments_id','like','%'.$get_sector.'%')->where('country_employments_id','like','%'.$get_country.'%')->orderBy('created_at','desc')->paginate(10);
        return view('user.employment',compact('employment','country','sector'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
