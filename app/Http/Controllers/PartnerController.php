<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CountryEmployment;
use App\SectorEmployment;
use App\Employment;
use App\User;
use App\Profile;
use Auth;
class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partner.index');
    }

    public function employmentIndex()
    {
        $data = Employment::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
        // dd($data);
        return view('partner.employment',compact('data'));
    }

    public function employmentCreate()
    {
        $country = CountryEmployment::all();
        $sector = SectorEmployment::all();
        return view('partner.employment_create',compact('country','sector'));
    }

    public function employmentStore(Request $request)
    {
        $a = $request->validate([
            'title' => ['required', 'string','min:10','max:100'],
            'sector_employments_id' => ['required'],
            'country_employments_id' => ['required'],
            'description' => ['required', 'string', 'min:10'],
            'sallery' => ['required','min:1','max:12'],
            'end_date' => ['required']
        ]);

        if($request->hasFile('cover')){
            $filenameWithExt = $request->file('cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('public/upload/partner_banner', $file_store_name);
        }else{
            $file_store_name = 'noimage.jpg';
        }

        $data = new Employment();
        $data->title = $a['title'];
        $data->sector_employments_id = $a['sector_employments_id'];
        $data->country_employments_id = $a['country_employments_id'];
        $data->description = $a['description'];
        $data->sallery = $a['sallery'];
        $data->end_date = $a['end_date'];
        $data->cover = $file_store_name;
        $data->user_id = Auth::user()->id;
        $data->is_flag = 1;
        $data->save();
        return redirect('partner/employment')->with('alert','Berhasil Ditambah');
    }

    public function employmentDetail($id)
    {
        $data = Employment::find($id);
        return view('partner.employment_detail',compact('data'));
    }

    public function employmentShow($id)
    {
        $country = CountryEmployment::all();
        $sector = SectorEmployment::all();
        $data = Employment::find($id);
        return view('partner.employment_update',compact('data','country','sector'));
    }

    public function employmentUpdate(Request $request, $id)
    {
        $a = $request->validate([
            'title' => ['required', 'string','min:10','max:100'],
            'sector_employments_id' => ['required'],
            'country_employments_id' => ['required'],
            'description' => ['required', 'string', 'min:10'],
            'sallery' => ['required','min:1','max:12'],
            'end_date' => ['required']
        ]);

        // dd($a);

        if($request->hasFile('cover')){
            $filenameWithExt = $request->file('cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('public/upload/partner_banner', $file_store_name);
        }else{
            $file_store_name = $request->get('old_cover');
        }

        $data = Employment::find($id);
        $data->title = $a['title'];
        $data->sector_employments_id = $a['sector_employments_id'];
        $data->country_employments_id = $a['country_employments_id'];
        $data->description = $a['description'];
        $data->sallery = $a['sallery'];
        $data->end_date = $a['end_date'];
        $data->cover = $file_store_name;
        $data->user_id = Auth::user()->id;
        $data->is_flag = 1;
        $data->save();
        return redirect('partner/employment')->with('alert','Berhasil Diubah');
    }

    public function applyIndex()
    {
        $data = Employment::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
        return view('partner.apply',compact('data'));
    }

    public function applyIntrest(Request $request, $id)
    {
        return view('partner.index');
    }

    public function memberIndex()
    {
        $data = User::where('role_id',3)->get();
        // dd($data);
        return view('partner.member',compact('data'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
