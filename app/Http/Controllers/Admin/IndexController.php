<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ratting;
use App\Partner;
use App\RegisterEmployment;
use App\Employment;
use DB;
class IndexController extends Controller
{
    public function index()
    {
        $query = 'SELECT a.name, a.ratting, count(b.id)as total_employment, count(c.id) as total_pmi
        FROM partners a
        left join employments b on b.partner_id = a.id
        left join register_employments c on b.id = c.employment_id
        group by a.id';
        $data = DB::select($query);

        // get employment
        $query_employment = 'SELECT count(b.id)as total_employment
            FROM partners a
            left join employments b on b.partner_id = a.id
            left join register_employments c on b.id = c.employment_id
        ';
        $total_employment = DB::select($query_employment);

        // get register
        $query_pmi = 'SELECT count(c.id) as total_pmi
            FROM partners a
            left join employments b on b.partner_id = a.id
            left join register_employments c on b.id = c.employment_id
        ';
        $total_pmi = DB::select($query_pmi);

        $employment = Employment::count();
        $register = RegisterEmployment::count();
        return view('pages.admin.index',compact('data','total_register_employment','total_pmi','total_employment'));
    }

    public function testimonial()
    {
        $data = Ratting::latest()->get();
        return view('pages.admin.testimonial',compact('data'));
    }

    public function testimonial_approv(Request $request, $id)
    {
        // approv data
        $data = Ratting::find($id);
        $data->is_approv = 'ya';
        $data->save();
        // count data
        $this_value = Ratting::where([['partner_id',$data->employment->partner->id],['is_approv', 'ya']])->get();
        $count = $this_value->count();
        $sum = $this_value->sum('value');
        // dd('sum '.$sum.' count '.$count.' ');
        $result = $sum/$count;
        // dd($result);
        $partner = Partner::find($data->employment->partner->id);
        $partner->ratting = $result;
        // $data->save();
        $partner->save();
        // redirect
        return redirect(route('admin-testimonial'))->with('alert','Testimonial telah disetujui.');
    }
}
