<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use App\CountryEmployment;
use App\RegisterEmployment;
use App\User;
use App\Partner;
use App\Employment;
use PDF;
use DB;
class UserController extends Controller
{
    public function report(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        // get partner relation
        $query = 'SELECT a.name, a.ratting, count(b.id)as total_employment, count(c.id) as total_pmi
            FROM partners a
            left join employments b on b.partner_id = a.id
            left join register_employments c on b.id = c.employment_id
            where b.end_date BETWEEN "'.$start.'" AND "'.$end.'"
            group by a.id
        ';
        $data = DB::select($query);

        // get employment
        $query_employment = 'SELECT count(b.id)as total_employment
            FROM partners a
            left join employments b on b.partner_id = a.id
            left join register_employments c on b.id = c.employment_id
            where b.end_date BETWEEN "'.$start.'" AND "'.$end.'"
        ';
        $total_employment = DB::select($query_employment);

        // get register
        $query_pmi = 'SELECT count(c.id) as total_pmi
            FROM partners a
            left join employments b on b.partner_id = a.id
            left join register_employments c on b.id = c.employment_id
            where b.end_date BETWEEN "'.$start.'" AND "'.$end.'"
        ';
        $total_pmi = DB::select($query_pmi);

        // run pdf
        $pdf = PDF::loadView('pages.report',compact('data','total_pmi','total_employment','start','end'));
	    return $pdf->stream('invoice.pdf');
    }

    public function index()
    {
        $data = Profile::latest()->get();
        return view('pages.admin.users',compact('data'));
    }

    public function show($id)
    {
        $data = Profile::find($id);
        $country = CountryEmployment::all();
        return view('pages.admin.users_detail',compact('data','country'));
    }

    public function update(Request $request, $id){
        $data = User::find($id);
        $data->is_approv = $request->get('is_approv');
        $data->save();

        if($data->is_approv == 'false' ){
            $status = 'Tidak Aktif';
        }else{
            $status = 'Aktif';
        }
        return redirect(route('admin-user'))->with('alert','Berhasil ubah status '.$data->profile->real_name.' menjadi '.$status.'.');
    }

    public function rekrutmen()
    {
        $data = RegisterEmployment::where('status','>','bekerja')->latest()->get();
        // dd($data);
        return view('pages.admin.progress',compact('data'));
    }
}
