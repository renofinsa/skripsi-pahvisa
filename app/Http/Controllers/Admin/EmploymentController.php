<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employment;
use App\SectorEmployment;
use App\CountryEmployment;
use Auth;
class EmploymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Employment::latest()->get();
        return view('pages.admin.employment',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sector = SectorEmployment::latest()->get();
        $country = CountryEmployment::latest()->get();
        return view('pages.admin.employment_create',compact('sector','country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'min:3','max:100'],
            'sallery' => ['required', 'string', 'min:1','max:8'],
            'country_employments_id' => ['required'],
            'sector_employments_id' => ['required'],
            'description' => ['required','string','min:5'],
            'end_date' => [],
        ]);

        $emloyment = new Employment();
        $emloyment->user_id = Auth::user()->id;
        $emloyment->title = $data['title'];
        $emloyment->sallery = $data['sallery'];
        $emloyment->country_employments_id = $data['country_employments_id'];
        $emloyment->sector_employments_id = $data['sector_employments_id'];
        $emloyment->description = $data['description'];
        $emloyment->end_date = $data['end_date'];
        $emloyment->save();

        return redirect(route('admin-employment'))->with('alert','Lowongan berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Employment::find($id);
        return view('pages.admin.employment_show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $emloyment = Employment::find($id);
        $emloyment->is_publish = $request->get('is_publish');
        $emloyment->save();

        return redirect(route('admin-employment'))->with('alert','Anda berhasil menanggapi lowongan '.$emloyment->title.' menjadi '.$emloyment->is_publish.'.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
