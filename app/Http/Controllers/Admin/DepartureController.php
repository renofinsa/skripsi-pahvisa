<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RegisterEmployment;
use App\Profile;
class DepartureController extends Controller
{
    public function index()
    {
        $data = RegisterEmployment::latest()->get();
        return view('pages.admin.departure',compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = RegisterEmployment::find($id);
        $data->status = $request->get('status');
        $data->save();

        $user_id = $data->user->profile->id;

        $user = Profile::find($user_id);
        $user->status = 'PMI';
        $user->save();

        return redirect()->back()->with('alert','Status berhasil diubah.');
    }
}
