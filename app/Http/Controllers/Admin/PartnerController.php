<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partner;
use App\User;
use App\CountryEmployment;
class PartnerController extends Controller
{
    public function index()
    {
        $data = Partner::latest()->get();
        return view('pages.admin.partner',compact('data'));
    }

    public function create()
    {
        $country = CountryEmployment::orderBy('name','asc')->get();
        return view('pages.admin.partner_create',compact('country'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'email' => ['required', 'string', 'email', 'max:50','min:5', 'unique:users'],
            'password' => ['required', 'string', 'max:60','min:5'],
            'name' => ['required', 'string', 'min:3','max:60'],
            'pic' => ['required', 'string', 'min:3','max:16'],
            'phone_number' => ['required', 'string', 'min:10','max:13'],
            'address' => ['required', 'string', 'min:5','max:50'],
            'country_employments_id' => [],
        ]);

        $user = new User();
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->role_id = 3;
        $user->save();

        $partner = new Partner();
        $partner->ratting = 0;
        $partner->user_id = $user->id;
        $partner->logo = 'partner_logo_default.png';
        $partner->name = $data['name'];
        $partner->pic = $data['pic'];
        $partner->phone_number = $data['phone_number'];
        $partner->address = $data['address'];
        $partner->country_employments_id = $data['country_employments_id'];
        $partner->save();

        return redirect(route('admin-partner'))->with('alert','PPTKIS berhasil dibuat.');
    }

    public function show($id)
    {
        $data = Partner::find($id);
        $country = CountryEmployment::orderBy('name','asc')->get();
        return view('pages.admin.partner_update',compact('country','data'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'min:3','max:60'],
            'pic' => ['required', 'string', 'min:3','max:16'],
            'phone_number' => ['required', 'string', 'min:10','max:13'],
            'address' => ['required', 'string', 'min:5','max:50'],
            'country_employments_id' => [],
        ]);

        $partner = Partner::find($id);
        $partner->name = $data['name'];
        $partner->pic = $data['pic'];
        $partner->phone_number = $data['phone_number'];
        $partner->address = $data['address'];
        $partner->country_employments_id = $data['country_employments_id'];
        $partner->save();

        return redirect(route('admin-partner'))->with('alert','PPTKIS berhasil diubah.');
    }
}
