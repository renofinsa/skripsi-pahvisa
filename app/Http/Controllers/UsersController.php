<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Partner;
use App\CountryEmployment;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userIndex()
    {
        $data = User::where('role_id',3)->get();
        return view('admin.user_index',compact('data'));
    }

    public function userShow($id)
    {
        $country = CountryEmployment::orderBy('name','asc')->get();
        $get = User::find($id)->where('role_id',3)->get();
        $data = $get[0];
        // dd($data->profile->country->id);
        return view('admin.user_update',compact('data','country'));
    }

    public function userUpdate(Request $request, $id)
    {
        // $data = User::find($id);
        // $data->
        // dd($data);
    }

    public function partnerIndex(){
        $data = User::where('role_id',2)->get();
        return view('admin.partner_index',compact('data'));
    }

    public function partnerCreate()
    {
        $country = CountryEmployment::orderBy('name','asc')->get();
        // dd($data->profile->country->id);
        return view('admin.partner_create',compact('country'));
    }

    public function partnerStore(Request $request)
    {

        $a = $request->validate([
            'email' => ['required', 'string', 'email', 'max:100', 'unique:users'],
            'cover' => ['required'],
            'name' => ['required', 'string','min:3'],
            'pic' => ['required', 'string', 'min:3'],
            'phone_number' => ['required','min:10','max:12'],
            'address' => ['required','min:10'],
            'country_employments_id' => ['required'],
        ]);

        if($request->hasFile('cover')){
            $filenameWithExt = $request->file('cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('public/upload/partner_logo', $file_store_name);
        }else{
            $file_store_name = 'noimage.jpg';
        }

        $data = new User;
        $data->email = $a['email'];
        $data->password = bcrypt('tobehero');
        $data->role_id = 2;
        $data->save();

        $partner = new Partner;
        $partner->user_id = $data->id;
        $partner->logo = $file_store_name;
        $partner->name = $a['name'];
        $partner->pic = $a['pic'];
        $partner->phone_number = $a['phone_number'];
        $partner->address = $a['address'];
        $partner->country_employments_id = $a['country_employments_id'];
        $partner->save();

        return redirect('/admin/partners')->with('alert','Berhasil ditambah.');
    }

    public function partnerShow($id)
    {
        $country = CountryEmployment::orderBy('name','asc')->get();
        $get = User::where([['role_id',2],['id',$id]])->get();
        $data = $get[0];
        // dd($data);
        return view('admin.partner_update',compact('data','country'));
    }

    public function partnerUpdate(Request $request, $id)
    {
        // dd($request);

        if($request->hasFile('cover')){
            $filenameWithExt = $request->file('cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('public/upload/partner_logo', $file_store_name);
            unlink('storage/upload/partner_logo/'.$request->get('old_cover'));
        }else{
            $file_store_name = $request->get('old_cover');
        }

        $data = User::find($id);
        // $partner = Partner::where('user_id',$id)->get();
        // dd($partner);
        $data->partner->logo = $file_store_name;
        $data->partner->name = $request->get('name');
        $data->partner->pic = $request->get('pic');
        $data->partner->phone_number = $request->get('phone_number');
        $data->partner->address = $request->get('address');
        $data->partner->country_employments_id = $request->get('country_employments_id');
        $data->partner->save();

        return redirect('/admin/partners')->with('alert','Berhasil diubah.');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
