<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Employment;
use App\SectorEmployment;
use App\CountryEmployment;
use App\User;
use App\Profile;
class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('authguest');
    // }

    public function index(Request $request)
    {
        $key = $request->get('search');
        $data = Employment::where([['is_publish','disetujui'],['title', 'LIKE', '%'.$key.'%']])->paginate(6);
        return view('pages.index',compact('data'));
    }

    public function show($id)
    {
        $data = Employment::find($id);
        if(count($data) < 1){
            return view('pages.404');
        }else{
            return view('pages.index-detail',compact('data'));
        }
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'real_name' => ['required', 'string', 'max:255','min:3'],
            'email' => ['required', 'string', 'email', 'max:50','min:5', 'unique:users'],
            'password' => ['required', 'string', 'min:5','max:60'],
            'ktp' => ['required', 'string', 'min:1','max:16'],
            'phone_number' => ['required', 'string', 'min:10','max:13'],
            'address' => ['required', 'string', 'min:5','max:50'],
            'education_school' => [],
            'education_start_year' => [],
            'education_end_year' => [],
            'experiences_company' => [],
            'experiences_position' => [],
            'experiences_classification' => [],
            'country_employments_id' => [],
        ]);

        $get_id = new User();
        $get_id->email = $data['email'];
        $get_id->password = bcrypt($data['password']);
        $get_id->role_id = 3;
        $get_id->save();

        $add = new Profile();
        $add->user_id = $get_id->id;
        $add->real_name = $data['real_name'];
        $add->card_id = $data['ktp'];
        $add->phone_number = $data['phone_number'];
        $add->education_school_name = $data['education_school'];
        $add->education_start_year = $data['education_start_year'];
        $add->education_end_year = $data['education_end_year'];
        $add->experiences_company = $data['experiences_company'];
        $add->experiences_position = $data['experiences_position'];
        $add->experiences_classification = $data['experiences_classification'];
        $add->country_employments_id = $data['country_employments_id'];
        $add->status = true;
        $add->uploads_card_id = null;
        $add->uploads_resume = null;
        $add->uploads_passport = null;
        $add->save();

        return redirect(route('login'))->with('alert','Kamu berhasil mendaftar CPMI, silakan masuk aplikasi untuk mendaftar pekerjaan.');
    }
}
