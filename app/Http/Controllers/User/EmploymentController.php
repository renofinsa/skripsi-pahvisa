<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RegisterEmployment;
use App\Employment;
use App\SaveEmployment;
use Auth;
class EmploymentController extends Controller
{
    public function index()
    {
        $data = SaveEmployment::where('user_id',Auth::user()->id)->get();
        return view('pages.user.employment',compact('data'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employment_id = $request->get('employment_id');
        $user_id = Auth::user()->id;
        $check = SaveEmployment::where([['user_id',$user_id],['employment_id',$employment_id]])->get();
        if(count($check) > 0){
            return redirect(route('user-employment-show',$employment_id))->with('alertWarning','Lowongan sudah tersimpan.');
        }else{
            $data = new SaveEmployment();
            $data->employment_id = $employment_id;
            $data->user_id = $user_id;
            $data->save();
            return redirect(route('user-employment-show',$employment_id))->with('alert','Lowongan berhasil disimpan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Employment::find($id);
        return view('pages.user.employment_show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SaveEmployment::find($id);
        $data->delete();
        return redirect(route('user-employment'))->with('alert','Lowongan berhasil dihapus.');
    }
}
