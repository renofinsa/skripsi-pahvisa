<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RegisterEmployment;
use App\Ratting;
use Auth;
class DepatureController extends Controller
{
    public function index()
    {
        $data = RegisterEmployment::where('user_id',Auth::user()->id)->get();
        return view('pages.user.depature',compact('data'));
    }

    public function show($id)
    {
        $data = RegisterEmployment::where('user_id',Auth::user()->id)->where('id',$id)->get();
        return view('pages.user.depature_show',compact('data'));
    }

    public function store(Request $request)
    {
        $employment_id = $request->get('employment_id');
        $user_id = Auth::user()->id;
        $check = RegisterEmployment::where([['user_id',$user_id],['employment_id',$employment_id]])->get();
        if(count($check) > 0){
            return redirect(route('user-employment-show',$employment_id))->with('alertWarning','Lowongan sudah terkirim.');
        }else{
            $data = new RegisterEmployment();
            $data->employment_id = $employment_id;
            $data->user_id = $user_id;
            $data->save();
            return redirect(route('user-employment-show',$employment_id))->with('alert','Lowongan berhasil dikirim.');
        }
    }

    public function sent_ratting(Request $request)
    {
        try {
            $check = Ratting::where([['user_id',Auth::user()->id],['employment_id',$request->get('employment_id')]])->get();
            if(count($check) > 0){
                return redirect(route('user-departure'))->with('alertWarning','Testimonial sudah diberikan.');
            }else{
                $data = new Ratting();
                $data->user_id = Auth::user()->id;
                $data->employment_id = $request->get('employment_id');
                $data->partner_id = $request->get('partner_id');
                $data->note = $request->get('note');
                $data->value = $request->get('value');
                $data->is_approv = 2;
                $data->save();
                return redirect(route('user-departure'))->with('alert','Testimonial berhasil dikirim');
            }
        } catch (\Throwable $th) {
            return redirect(route('user-departure'))->with('alertWarning','Terjadi kesalahan');
        }
    }

    public function destroy($id)
    {
        $data = RegisterEmployment::find($id);
        $data->delete();
        return redirect(route('user-depature'))->with('alert','Lowongan telah dibatalkan.');
    }
}
