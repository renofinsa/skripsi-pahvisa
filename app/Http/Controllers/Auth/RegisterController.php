<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Profile;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected function redirectTo ()
    {
        if (Auth::user()->role_id == 1){
            return redirect('/admin');
        } else if (Auth::user()->role_id == 2) {
            return redirect('/partner');
        } else if (Auth::user()->role_id == 3) {
            return redirect('/user');
        }else{
            return redirect('/logout');
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'real_name' => ['required', 'string', 'max:255','min:3'],
            'email' => ['required', 'string', 'email', 'max:50','min:5', 'unique:users'],
            'password' => ['required', 'string', 'min:5','max:60'],
            'ktp' => ['required', 'string', 'min:1','max:16'],
            'phone_number' => ['required', 'string', 'min:10','max:13'],
            'address' => ['required', 'string', 'min:5','max:50'],
            'education_school' => [],
            'education_start_year' => [],
            'education_end_year' => [],
            'experiences_company' => [],
            'experiences_position' => [],
            'experiences_classification' => [],
            'country_employments_id' => [],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {


    }
}
