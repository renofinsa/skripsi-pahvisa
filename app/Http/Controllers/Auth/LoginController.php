<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected function redirectTo ()
    {
        // dd(Auth::user()->is_approv);
        if(Auth::user()->is_approv == 'false'){
            return '/200';
        }else{
            if(Auth::user()->role_id == 1){
                return '/admin';
            } else if (Auth::user()->role_id == 2) {
                return '/partner';
            } else if (Auth::user()->role_id == 3) {
                return '/user';
            }else{
                return '/logout';
            }
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect('/login');
    }

    public function logout200(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect('/200');
    }
}
