<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use App\CountryEmployment;

class UserController extends Controller
{
    public function index()
    {
        $data = Profile::latest()->get();
        return view('pages.partner.users',compact('data'));
    }

    public function show($id)
    {
        $data = Profile::find($id);
        $country = CountryEmployment::all();
        return view('pages.partner.users_detail',compact('data','country'));
    }
}
