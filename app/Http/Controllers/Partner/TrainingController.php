<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employment;
use App\RegisterEmployment;
use Auth;
use DB;
class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Employment::where('partner_id',Auth::user()->partner2->id)->get();
        return view('pages.partner.training',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = Employment::find($id);
        return view('pages.partner.training_create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'last_join' => ['required'],
            'description' => ['required', 'string', 'min:10'],
        ]);

        $training = new training();
        $training->employment_id = $request->get('id');
        $training->partner_id = Auth::user()->partner2->id;
        $training->last_join = $data['last_join'];
        $training->description = $data['description'];
        $training->save();
        return redirect(route('partner-training'))->with('alert','Jadwal pelatihan berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Employment::find($id);
        return view('pages.partner.training_update',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function departure()
    {
        $data = Auth::user()->partner2->employment2->register;
        return view('pages.partner.departure',compact('data'));
    }

    public function participant($id)
    {
        $this_data = Employment::find($id);
        $data = RegisterEmployment::where('employment_id',$id)->get();
        return view('pages.partner.participant',compact('data','this_data'));
    }

    public function participant_progress(Request $request, $id)
    {
        $data = RegisterEmployment::find($id);
        if($data->status == 'bekerja'){
            return redirect()->back()->with('alertWarning','Status bekerja tidak dapat diubah.');
        }else{
            $data->status = $request->get('status');
            $data->save();
            return redirect()->back()->with('alert','Status berhasil diubah.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'last_join' => ['required'],
            'description' => ['required', 'string', 'min:10'],
        ]);

        $training = training::find($id);
        $training->last_join = $data['last_join'];
        $training->description = $data['description'];
        $training->save();
        return redirect(route('partner-training'))->with('alert','Jadwal pelatihan berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
