<?php

namespace App\Http\Controllers\Partner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employment;
use App\SectorEmployment;
use App\CountryEmployment;
use Auth;
class EmploymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Employment::where('partner_id',Auth::user()->partner2->id)->get();
        return view('pages.partner.index',compact('data'));
    }

    public function detail($id)
    {
        $data = Employment::find($id);
        if(count($data) < 1){
            return view('pages.404');
        }else{
            return view('pages.partner.employment_show',compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sector = SectorEmployment::latest()->get();
        $country = CountryEmployment::latest()->get();
        return view('pages.partner.employment_create',compact('sector','country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'min:3','max:100'],
            'sallery' => ['required', 'string', 'min:1','max:8'],
            'country_employments_id' => ['required'],
            'sector_employments_id' => ['required'],
            'description' => ['required','string','min:5'],
            'needed' => ['required'],
            'end_date' => [],
        ]);

        if($request->hasFile('cover')){
            $filenameWithExt = $request->file('cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('public/upload/partner_banner', $file_store_name);
        }else{
            $file_store_name = 'default-banner.png';
        }

        $emloyment = new Employment();
        $emloyment->partner_id = Auth::user()->partner2->id;
        $emloyment->title = $data['title'];
        $emloyment->sallery = $data['sallery'];
        $emloyment->country_employments_id = $data['country_employments_id'];
        $emloyment->sector_employments_id = $data['sector_employments_id'];
        $emloyment->description = $data['description'];
        $emloyment->end_date = $data['end_date'];
        $emloyment->needed = $data['needed'];
        $emloyment->cover = $file_store_name;
        $emloyment->is_publish = 1;
        $emloyment->save();
        return redirect(route('partner-employment'))->with('alert','Lowongan berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sector = SectorEmployment::latest()->get();
        $country = CountryEmployment::latest()->get();
        $data = Employment::find($id);
        return view('pages.partner.employment_show',compact('sector','country','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = SectorEmployment::latest()->get();
        $country = CountryEmployment::latest()->get();
        $data = Employment::find($id);
        return view('pages.partner.employment_update',compact('sector','country','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'min:3','max:100'],
            'sallery' => ['required', 'string', 'min:1','max:8'],
            'country_employments_id' => ['required'],
            'sector_employments_id' => ['required'],
            'description' => ['required','string','min:5'],
            'needed' => ['required'],
            'end_date' => [],
        ]);

        if($request->hasFile('cover')){
            $filenameWithExt = $request->file('cover')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('public/upload/partner_banner', $file_store_name);
            if($request->get('old_cover') !== 'default-banner.png'){
                unlink('storage/upload/partner_banner/'.$request->get('old_cover').'');
            }
        }else{
            $file_store_name = $request->get('old_cover');
        }

        $emloyment = Employment::find($id);
        $emloyment->partner_id = Auth::user()->partner2->id;
        $emloyment->title = $data['title'];
        $emloyment->sallery = $data['sallery'];
        $emloyment->country_employments_id = $data['country_employments_id'];
        $emloyment->sector_employments_id = $data['sector_employments_id'];
        $emloyment->description = $data['description'];
        $emloyment->end_date = $data['end_date'];
        $emloyment->needed = $data['needed'];
        $emloyment->cover = $file_store_name;
        $emloyment->is_publish = 1;
        $emloyment->save();
        return redirect(route('partner-employment'))->with('alert','Lowongan berhasil diubah, menunggu persetujuan APJATI');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
