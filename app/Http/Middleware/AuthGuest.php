<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        try {
            if(Auth::user() == null){
                return redirect('/');
            }else{
                return $next($request);
            }
        } catch (\Throwable $th) {
            return redirect('/');
        }
    }
}
