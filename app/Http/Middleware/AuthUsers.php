<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        try {
            if(Auth::guard($guard)->user()->is_approv == 'false'){
                return redirect('/200');
            }else{
                if(Auth::guard($guard)->user()->role_id == 3){
                    return $next($request);
                }else{
                    return redirect('/user');
                }
            }
        } catch (\Throwable $th) {
            return redirect('/logout');
        }
    }
}
