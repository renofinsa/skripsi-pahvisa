<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        try {
            if(Auth::guard($guard)->user()->role_id == 1){
                return $next($request);
            }else{
                return redirect('/admin');
            }
        } catch (\Throwable $th) {
            return redirect('/logout');
        }
    }
}
