<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    public function country(){
        return $this->belongsTo(CountryEmployment::class,'country_employments_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function employment(){
        return $this->belongsTo(Employment::class,'partner_id');
    }
    public function employment2(){
        return $this->hasMany(Employment::class,'partner_id');
    }
}
