<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterEmployment extends Model
{
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function employment(){
        return $this->belongsTo(Employment::class,'employment_id');
    }
}
