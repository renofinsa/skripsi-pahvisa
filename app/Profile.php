<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //

    public function user(){
        return $this->belongsTo(User::class,'id');
    }

    public function user2(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function country(){
        return $this->belongsTo(CountryEmployment::class,'country_employments_id');
    }
    protected $fillable = [
        'user_id',
        'real_name',
        'card_id',
        'phone_number',
        'education_school_name',
        'education_start_year',
        'education_end_year',
        'experiences_company',
        'experiences_position',
        'experiences_classification',
        'country_employments_id',
        'status',
        'uploads_card_id',
        'uploads_resume',
        'uploads_passport'
    ];
}














